package at.ac.tuwien.ifs.sge.gui.helper;

import java.io.*;

public class PrintStreamInterceptor extends PrintStream {

    private final PrintStream interceptionPrintStream;
    private PipedInputStream pipedInputStream;

    public PrintStreamInterceptor(PrintStream out) {
        super(out);
        PipedOutputStream pipedOutputStream = new PipedOutputStream();
        interceptionPrintStream = new PrintStream(pipedOutputStream);
        try {
            pipedInputStream = new RevivablePipedInputStream(pipedOutputStream, 65536);
        } catch (IOException ignored) {}
    }

    public InputStream getInputStream() {
        return pipedInputStream;
    }

    @Override
    public void write(int b) {
        super.write(b);
        interceptionPrintStream.write(b);

    }

    @Override
    public void write(byte[] buf, int off, int len) {
        super.write(buf, off, len);
        interceptionPrintStream.write(buf, off, len);

    }

    @Override
    public void println() {
        super.println();
        interceptionPrintStream.println();

    }
}
