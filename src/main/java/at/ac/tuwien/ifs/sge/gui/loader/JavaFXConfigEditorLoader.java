package at.ac.tuwien.ifs.sge.gui.loader;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.gui.factories.JavaFXConfigEditorFactory;
import at.ac.tuwien.ifs.sge.gui.interfaces.JavaFXConfigEditor;

import java.lang.reflect.Constructor;
import java.net.URLClassLoader;

public class JavaFXConfigEditorLoader {
    private Class<JavaFXConfigEditor> configEditorClass;

    private final String configEditorClassName;

    private final URLClassLoader classLoader;

    private final Logger log;


    public JavaFXConfigEditorLoader(URLClassLoader classLoader, String configEditorClassName, Logger log) {
        this.classLoader = classLoader;
        this.configEditorClassName = configEditorClassName;
        this.log = log;
    }

    @SuppressWarnings("unchecked")
    public JavaFXConfigEditorFactory newConfigEditorFactory(GameConfiguration initialConfiguration) {
        log.tra_("Loading config editor class...");
        try {
            configEditorClass = (Class<JavaFXConfigEditor>) classLoader
                    .loadClass(configEditorClassName);

            Constructor<JavaFXConfigEditor> configEditorConstructor = configEditorClass
                    .getConstructor(GameConfiguration.class, Logger.class);

            log._trace(", done.");
            return new JavaFXConfigEditorFactory(configEditorConstructor, initialConfiguration, log);
        } catch (ClassNotFoundException e) {
            log._trace(", failed.");
            log.error("Could not find class of config editor.");
            log.printStackTrace(e);
            throw new IllegalStateException("Could not find class of config editor.");
        } catch (NoSuchMethodException e) {
            log._trace(", failed.");
            log.error("Could not find required constructors of config editor.");
            log.printStackTrace(e);
            throw new IllegalStateException("Could not find required constructors of config editor.");
        }
    }
}
