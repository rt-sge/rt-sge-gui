package at.ac.tuwien.ifs.sge.gui.views.game;

import javafx.scene.Node;

public interface GameView {

    void dispose();

    void stop();

    void attachEngineConsole(Node node, boolean withVisualization);
}
