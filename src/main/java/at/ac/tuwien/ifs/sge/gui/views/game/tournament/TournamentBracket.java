package at.ac.tuwien.ifs.sge.gui.views.game.tournament;

import at.ac.tuwien.ifs.sge.core.engine.game.tournament.Tournament;
import at.ac.tuwien.ifs.sge.core.engine.game.tournament.TournamentMode;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

import java.io.IOException;

abstract public class TournamentBracket {

    public static TournamentBracket fromTournamentMode(TournamentMode mode) throws IOException {
        switch (mode) {
            default -> {
                FXMLLoader tableBracketViewLoader = new FXMLLoader(TableBracketViewController.class.getResource("table_bracket_view.fxml"));
                tableBracketViewLoader.load();
                return tableBracketViewLoader.<TableBracketViewController>getController();
            }
        }
    }

    public abstract void setup(Tournament tournament, Logger log);

    public abstract Node getRootNode();

    public abstract void dispose();

}
