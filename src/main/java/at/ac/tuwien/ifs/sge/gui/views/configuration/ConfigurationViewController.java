package at.ac.tuwien.ifs.sge.gui.views.configuration;

import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.agent.HumanAgent;
import at.ac.tuwien.ifs.sge.core.agent.MemorySpecifications;
import at.ac.tuwien.ifs.sge.core.engine.factory.MatchFactory;
import at.ac.tuwien.ifs.sge.core.engine.factory.RealTimeMatchFactory;
import at.ac.tuwien.ifs.sge.core.engine.factory.TurnBasedMatchFactory;
import at.ac.tuwien.ifs.sge.core.engine.game.match.RealTimeMatch;
import at.ac.tuwien.ifs.sge.core.engine.game.match.TurnBasedMatch;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.factory.GameFactory;
import at.ac.tuwien.ifs.sge.core.engine.game.match.Match;
import at.ac.tuwien.ifs.sge.core.engine.game.tournament.Tournament;
import at.ac.tuwien.ifs.sge.core.engine.game.tournament.TournamentMode;
import at.ac.tuwien.ifs.sge.core.engine.loader.GameLoader;
import at.ac.tuwien.ifs.sge.core.game.*;
import at.ac.tuwien.ifs.sge.core.util.Util;
import at.ac.tuwien.ifs.sge.gui.factories.JavaFXConfigEditorFactory;
import at.ac.tuwien.ifs.sge.gui.interfaces.JavaFXConfigEditor;
import at.ac.tuwien.ifs.sge.gui.loader.JavaFXConfigEditorLoader;
import at.ac.tuwien.ifs.sge.gui.util.IO;
import at.ac.tuwien.ifs.sge.gui.util.UI;
import at.ac.tuwien.ifs.sge.gui.wrapper.FileWrapper;
import io.reactivex.rxjava3.subjects.PublishSubject;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.jar.Attributes;
import java.util.jar.JarFile;

public class ConfigurationViewController implements Initializable {

    private static final String SGE_TYPE = "Sge-Type";
    private static final String SGE_TYPE_GAME = "game";
    private static final String SGE_TYPE_AGENT = "agent";
    private static final String SGE_AGENT_NAME = "Agent-Name";
    private static final String SGE_GAME_CLASS = "Game-Class";
    private static final String SGE_REALTIME = "Sge-Realtime";
    private static final ObservableList<Character> BYTE_IDENTIFER_LIST = FXCollections.observableArrayList('k', 'M', 'G');


    public final PublishSubject<Match<?, ?>> startMatchSubject = PublishSubject.create();
    public final PublishSubject<Tournament> startTournamentSubject = PublishSubject.create();
    public final PublishSubject<JavaFXConfigEditor> openConfigEditorSubject = PublishSubject.create();

    private final ObservableList<FileWrapper> gameFiles = FXCollections.observableArrayList();
    private final ObservableList<FileWrapper> agentFiles = FXCollections.observableArrayList();
    private final ObservableList<Agent> selectedAgents = FXCollections.observableArrayList();
    private final BooleanProperty gameSelected = new SimpleBooleanProperty(false);
    private final StringProperty configEditorClassName = new SimpleStringProperty(null);

    private Logger log;
    private ExecutorService threadPool;

    private GameLoader<Game<Object, Object>> gameLoader = null;
    private GameFactory<Game<Object, Object>> gameFactory = null;

    private JavaFXConfigEditorFactory configEditorFactory;

    private GameConfiguration gameConfiguration;

    private Stage stage;
    private int agentIdCnt = 0;

    private TurnBasedConfigViewController turnBasedConfigViewController;
    private VBox turnBasedConfigViewRoot;

    private RealtimeConfigViewController realtimeConfigViewController;
    private VBox realtimeConfigViewRoot;

    @FXML
    private AnchorPane specificConfigAnchorPane;
    @FXML
    private AnchorPane consoleAnchorPane;
    @FXML
    private ListView<FileWrapper> gameListView;
    @FXML
    private ListView<FileWrapper> agentListView;
    @FXML
    private ListView<Agent> selectedAgentListView;
    @FXML
    private TextField gameConfigTextField;
    @FXML
    private TextField humanNameTextField;
    @FXML
    private TextField playersPerMatchTextField;
    @FXML
    private TextField minHeapSizeTextField;
    @FXML
    private TextField maxHeapSizeTextField;
    @FXML
    private TextField threadStackSizeTextField;
    @FXML
    private Button loadGameBtn;
    @FXML
    private Button openConfigEditorBtn;
    @FXML
    private Button addAgentBtn;
    @FXML
    private Button addHumanBtn;
    @FXML
    private Button removeAgentBtn;
    @FXML
    private Button clearConfigurationBtn;
    @FXML
    private Button startMatchBtn;
    @FXML
    private Button startTournamentBtn;
    @FXML
    private Label gameLabel;
    @FXML
    private Label minNrOfPlayersLabel;
    @FXML
    private Label maxNrOfPlayersLabel;
    @FXML
    private Label realtimeLabel;
    @FXML
    private Label gameConfigLabel;
    @FXML
    private CheckBox showVisualizationCheckBox;
    @FXML
    private CheckBox createAgentLogFilesCheckBox;
    @FXML
    private ChoiceBox<TournamentMode> tournamentChoiceBox;
    @FXML
    private ChoiceBox<Character> minHeapSizeChoiceBox;
    @FXML
    private ChoiceBox<Character> maxHeapSizeChoiceBox;
    @FXML
    private ChoiceBox<Character> threadStackSizeChoiceBox;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadSpecificConfigViews();
        setupControls();
        setupBindings();
    }

    public void setup(Stage stage, Logger log, ExecutorService threadPool) {
        this.stage = stage;
        this.log = log;
        this.threadPool = threadPool;
    }

    public void attachConsole(Node consoleNode) {
        consoleAnchorPane.getChildren().add(consoleNode);
    }
    public void detachConsole() {
        consoleAnchorPane.getChildren().clear();
    }

    public void loadGameFiles(String gameDirectory) {
        var wrappers = loadFiles(gameDirectory, SGE_TYPE_GAME);
        gameFiles.setAll(wrappers);
    }

    public void loadAgentFiles(String agentDirectory) {
        var wrappers = loadFiles(agentDirectory, SGE_TYPE_AGENT);
        agentFiles.setAll(wrappers);
    }

    public void loadBoardFile() {
        File boardFile = IO.openConfigFileChooser(stage);
        if (boardFile != null) {
            gameConfigTextField.setText(boardFile.getAbsolutePath());
        }
    }

    public void loadGame() {
        var selectedWrapper = gameListView.getSelectionModel().getSelectedItem();
        var file = selectedWrapper.getFile();
        var attributes = selectedWrapper.getAttributes();

        var gameConfigText = gameConfigTextField.getText();
        String gameConfigurationString = null;
        if (gameConfigText.isEmpty())
            gameConfigText = "Default Configuration";
        else
            gameConfigurationString = gameConfigText;

        var config = GameConfiguration.fromString(gameConfigurationString);

        URL gameUrl = null;
        try {
            gameUrl = file.toURI().toURL();
        } catch (MalformedURLException e) {
            return;
        }

        var gameClassName = attributes.getValue(SGE_GAME_CLASS);
        if (gameClassName == null) {
            UI.showAlertAndWait(stage,"Error while loading game!", "Attributes don't contain game class name!", Alert.AlertType.ERROR);
            return;
        }

        var classLoader = URLClassLoader.newInstance(new URL[] {gameUrl});

        gameLoader = new GameLoader<>(gameClassName,
                classLoader, log);

        loadGame(config, gameConfigText);
    }

    public void loadGame(GameConfiguration configuration, String gameConfigText) {

        gameConfiguration = configuration;

        try {
            gameFactory = gameLoader.newGameFactory(gameConfiguration);
        } catch (IllegalStateException e) {
            UI.showAlertAndWait(stage,"Error while loading game!", e.getMessage(), Alert.AlertType.ERROR);
            return;
        }

        if (gameFactory != null) {
            specificConfigAnchorPane.getChildren().clear();
            specificConfigAnchorPane.getChildren().add(gameFactory.isRealtime() ? realtimeConfigViewRoot : turnBasedConfigViewRoot);
            selectedAgents.clear();
            agentIdCnt = 0;

            gameLabel.setText(gameLoader.getGameClass().getSimpleName());
            minNrOfPlayersLabel.setText(Integer.toString(gameFactory.getMinimumNumberOfPlayers()));
            maxNrOfPlayersLabel.setText(Integer.toString(gameFactory.getMaximumNumberOfPlayers()));
            realtimeLabel.setText(gameFactory.isRealtime() ? "Yes" : "No");
            gameConfigLabel.setText(gameConfigText);
            configEditorClassName.set(gameFactory.getConfigEditorClassName());

            if (configEditorClassName.isNotNull().getValue()) {
                var configEditorLoader = new JavaFXConfigEditorLoader(gameLoader.getURLClassLoader(), configEditorClassName.getValue(), log);
                configEditorFactory = configEditorLoader.newConfigEditorFactory(gameFactory.getGameConfiguration());
            }

            gameSelected.setValue(true);
        }
    }

    public void openConfigEditor() {
        openConfigEditorSubject.onNext(configEditorFactory.newInstance());
    }

    public void addAgent() {
        var selectedWrapper = agentListView.getSelectionModel().getSelectedItem();
        var file = selectedWrapper.getFile();
        var attributes = selectedWrapper.getAttributes();

        var agentName = attributes.getValue(SGE_AGENT_NAME);
        if (agentName == null) {
            UI.showAlertAndWait(stage,"Error while adding agent!", "Attributes don't contain agent name!", Alert.AlertType.ERROR);
            return;
        }

        boolean realtime = Boolean.parseBoolean(attributes.getValue(SGE_REALTIME));
        if (gameFactory.isRealtime() && !realtime) {
            UI.showAlertAndWait(stage,"Error while adding agent!", "Game is in realtime but agent is for turn based!", Alert.AlertType.ERROR);
            return;
        } else if (!gameFactory.isRealtime() && realtime) {
            UI.showAlertAndWait(stage,"Error while adding agent!", "Game is turn based but agent is for realtime!", Alert.AlertType.ERROR);
            return;
        }
        var agent = new Agent(agentIdCnt, null, false, agentName, file, realtime);
        selectedAgents.add(agent);
        agentIdCnt++;
    }

    public void addHuman() {
        var name = humanNameTextField.getText();
        if (name.isEmpty()) {
            name = "Human";
        }
        var humanAgentClassName = gameFactory.getHumanAgentClassName();
        if (humanAgentClassName != null)
            selectedAgents.add(new HumanAgent(agentIdCnt,
                    null,
                    name,
                    gameFactory.isRealtime(),
                    gameFactory.getHumanAgentClassName(),
                    gameLoader.getURLClassLoader()));
        else
            selectedAgents.add(new HumanAgent(agentIdCnt,
                    null,
                    name,
                    gameFactory.isRealtime()));
        agentIdCnt++;
    }

    public void removeSelectedAgent() {
        var agent = selectedAgentListView.getSelectionModel().getSelectedItem();
        selectedAgents.remove(agent);
        agentIdCnt = 0;
        selectedAgents.forEach(a -> {
            a.setAgentId(agentIdCnt);
            selectedAgentListView.fireEvent(new ListView.EditEvent<>(selectedAgentListView, ListView.editCommitEvent(), a, agentIdCnt));
            agentIdCnt++;
        });
    }

    public void clearAgentConfiguration() {
        selectedAgents.clear();
        agentIdCnt = 0;
    }

    public void startMatch() {

        if (!setMemorySpecifications()) return;

        var numberOfPlayers = selectedAgents.size();
        if (numberOfPlayers < gameFactory.getMinimumNumberOfPlayers()) {
            UI.showAlertAndWait(stage,"Could not start match!", "There are not enough agents in the agent configuration.", Alert.AlertType.ERROR);
            return;
        } else if (numberOfPlayers > gameFactory.getMaximumNumberOfPlayers()) {
            UI.showAlertAndWait(stage,"Could not start match!", "There are too many agents in the agent configuration", Alert.AlertType.ERROR);
            return;
        }

        var playerMap = Util.getPlayerMap(selectedAgents);
        var game = gameFactory.newInstance(gameConfiguration, numberOfPlayers);
        Match<?, ?> match;

        if (game.isRealtime()) {
            match = new RealTimeMatch<>((RealTimeGame<?, ?>) game,
                    playerMap,
                    log,
                    threadPool,
                    realtimeConfigViewController.getTimeLimit(),
                    false);
        } else {
            var timeUnit = turnBasedConfigViewController.getTimeUnit();
            var computationTime = turnBasedConfigViewController.getComputationTime();
            var maxActions = turnBasedConfigViewController.getMaxActions();
            match = new TurnBasedMatch<>((TurnBasedGame<?, ?>) game,
                    playerMap,
                    computationTime,
                    timeUnit,
                    log,
                    threadPool,
                    maxActions,
                    false);
        }

        startMatchSubject.onNext(match);
    }

    public void startTournament() {

        if (!setMemorySpecifications()) return;

        var tournamentMode = tournamentChoiceBox.getValue();

        int min = Math
                .max(gameFactory.getMinimumNumberOfPlayers(), tournamentMode.getMinimumPerRound());
        int max = Math
                .min(gameFactory.getMaximumNumberOfPlayers(), tournamentMode.getMaximumPerRound());

        var numberOfPlayersPerMatch = min;
        try {
            numberOfPlayersPerMatch = Integer.parseInt(playersPerMatchTextField.getText());
        } catch (NumberFormatException ignored) {}

        if (numberOfPlayersPerMatch < min) {
            UI.showAlertAndWait(stage,"Could not start tournament!", "Too few players per match.", Alert.AlertType.ERROR);
            return;
        } else if (numberOfPlayersPerMatch > max) {
            UI.showAlertAndWait(stage,"Could not start tournament!", "Too many players per match.", Alert.AlertType.ERROR);
            return;
        }
        var numberOfPlayers = selectedAgents.size();
        if (numberOfPlayers < numberOfPlayersPerMatch) {
            UI.showAlertAndWait(stage,"Could not start tournament!", "There are not enough agents in the agent configuration.", Alert.AlertType.ERROR);
            return;
        }

        MatchFactory matchFactory;
        if (gameFactory.isRealtime()) {
            matchFactory = new RealTimeMatchFactory(
                    gameFactory,
                    gameConfiguration,
                    numberOfPlayersPerMatch,
                    log,
                    threadPool,
                    realtimeConfigViewController.getTimeLimit(),
                    false
            );
        } else {
            var timeUnit = turnBasedConfigViewController.getTimeUnit();
            var computationTime = turnBasedConfigViewController.getComputationTime();
            var maxActions = turnBasedConfigViewController.getMaxActions();
            matchFactory = new TurnBasedMatchFactory(
                    gameFactory,
                    gameConfiguration,
                    numberOfPlayersPerMatch,
                    log,
                    threadPool,
                    false,
                    computationTime,
                    timeUnit,
                    maxActions
            );
        }
        var tournament = tournamentMode
                .getTournament(
                        matchFactory,
                        selectedAgents,
                        numberOfPlayersPerMatch,
                        log
                );

        startTournamentSubject.onNext(tournament);
    }

    public boolean shouldShowVisualization() {
        return showVisualizationCheckBox.isSelected();
    }

    public boolean shouldCreateAgentLogFiles() {
        return createAgentLogFilesCheckBox.isSelected();
    }

    private boolean setMemorySpecifications() {
        try {
            var memorySpecs = getMemorySpecifications();
            for (var agent : selectedAgents) {
                agent.setMemorySpecifications(memorySpecs);
            }
            return true;
        } catch (IllegalArgumentException e) {
            UI.showAlertAndWait(stage,"Could not start match!",  e.getMessage(), Alert.AlertType.ERROR);
            return false;
        }
    }

    private MemorySpecifications getMemorySpecifications() {
        var minHeapSize = minHeapSizeTextField.getText() + minHeapSizeChoiceBox.getValue();
        var maxHeapSize = maxHeapSizeTextField.getText() + maxHeapSizeChoiceBox.getValue();
        var threadStackSize = threadStackSizeTextField.getText() + threadStackSizeChoiceBox.getValue();
        return new MemorySpecifications(minHeapSize, maxHeapSize, threadStackSize);
    }

    private void loadSpecificConfigViews() {
        FXMLLoader turnBasedConfigViewLoader = new FXMLLoader(TurnBasedConfigViewController.class.getResource("turnbased_config_view.fxml"));
        try {
            turnBasedConfigViewRoot = turnBasedConfigViewLoader.load();
            turnBasedConfigViewController = turnBasedConfigViewLoader.getController();
        } catch (IOException e) {
            log.printStackTrace(e);
            log.error("Could not load turn based config view!");
        }

        FXMLLoader realtimeConfigViewLoader = new FXMLLoader(RealtimeConfigViewController.class.getResource("realtime_config_view.fxml"));
        try {
            realtimeConfigViewRoot = realtimeConfigViewLoader.load();
            realtimeConfigViewController = realtimeConfigViewLoader.getController();
        } catch (IOException e) {
            log.printStackTrace(e);
            log.error("Could not load realtime config view!");
        }
    }

    private void setupControls() {
        gameListView.setItems(gameFiles);
        agentListView.setItems(agentFiles);
        selectedAgentListView.setItems(selectedAgents);

        tournamentChoiceBox.setItems(FXCollections.observableArrayList(TournamentMode.values()));
        tournamentChoiceBox.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> playersPerMatchTextField.setText(Integer.toString(newValue.getMinimumPerRound())));
        tournamentChoiceBox.getSelectionModel().select(TournamentMode.ROUND_ROBIN);

        minHeapSizeChoiceBox.setItems(BYTE_IDENTIFER_LIST);
        maxHeapSizeChoiceBox.setItems(BYTE_IDENTIFER_LIST);
        threadStackSizeChoiceBox.setItems(BYTE_IDENTIFER_LIST);
        minHeapSizeChoiceBox.setValue(Agent.DEFAULT_MIN_HEAP_SIZE.charAt(Agent.DEFAULT_MIN_HEAP_SIZE.length() - 1));
        maxHeapSizeChoiceBox.setValue(Agent.DEFAULT_MAX_HEAP_SIZE.charAt(Agent.DEFAULT_MAX_HEAP_SIZE.length() - 1));
        threadStackSizeChoiceBox.setValue(Agent.DEFAULT_THREAD_STACK_SIZE.charAt(Agent.DEFAULT_THREAD_STACK_SIZE.length() - 1));

        minHeapSizeTextField.setText(Agent.DEFAULT_MIN_HEAP_SIZE.substring(0, Agent.DEFAULT_MIN_HEAP_SIZE.length() - 1));
        maxHeapSizeTextField.setText(Agent.DEFAULT_MAX_HEAP_SIZE.substring(0, Agent.DEFAULT_MAX_HEAP_SIZE.length() - 1));
        threadStackSizeTextField.setText(Agent.DEFAULT_THREAD_STACK_SIZE.substring(0, Agent.DEFAULT_THREAD_STACK_SIZE.length() - 1));
    }


    private void setupBindings() {
        loadGameBtn.disableProperty()
                .bind(Bindings.size(gameFiles).lessThan(1)
                        .or(Bindings.size(gameListView.getSelectionModel().getSelectedItems()).lessThan(1)));
        openConfigEditorBtn.disableProperty()
                .bind(configEditorClassName.isNull());
        addAgentBtn.disableProperty()
                .bind(gameSelected.not()
                        .or(Bindings.size(agentFiles).lessThan(1))
                        .or(Bindings.size(agentListView.getSelectionModel().getSelectedItems()).lessThan(1)));
        addHumanBtn.disableProperty()
                .bind(gameSelected.not());
        removeAgentBtn.disableProperty()
                .bind(Bindings.size(selectedAgents).lessThan(1)
                        .or(Bindings.size(selectedAgentListView.getSelectionModel().getSelectedItems()).lessThan(1)));
        clearConfigurationBtn.disableProperty()
                .bind(Bindings.size(selectedAgents).lessThan(1));

        startMatchBtn.disableProperty()
                .bind(gameSelected.not());

        startTournamentBtn.disableProperty()
                .bind(gameSelected.not());
    }


    private List<FileWrapper> loadFiles(String directoryPath, String sgeType) {
        var loadedFiles = new ArrayList<FileWrapper>();
        if (directoryPath == null) return loadedFiles;

        var directory = new File(directoryPath);
        var files = directory.listFiles();
        if (files == null) return loadedFiles;
        for (File file : files) {
            if (file.exists() && file.isFile()) {
                JarFile jarFile;
                try {
                    jarFile = new JarFile(file);
                } catch (IOException e) {
                    continue;
                }

                Attributes attributes;
                try {
                    attributes = jarFile.getManifest().getMainAttributes();
                } catch (IOException e) {
                    continue;
                }

                String type = attributes.getValue(SGE_TYPE);

                if (type == null) {
                    continue;
                }

                if (sgeType.equalsIgnoreCase(type)) {
                    loadedFiles.add(new FileWrapper(file, attributes));
                }
            }
        }
        return loadedFiles;
    }


}
