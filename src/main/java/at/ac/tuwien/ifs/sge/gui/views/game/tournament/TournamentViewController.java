package at.ac.tuwien.ifs.sge.gui.views.game.tournament;

import at.ac.tuwien.ifs.sge.core.engine.game.MatchResult;
import at.ac.tuwien.ifs.sge.core.engine.game.match.Match;
import at.ac.tuwien.ifs.sge.core.engine.game.tournament.Tournament;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.gui.views.game.GameView;
import at.ac.tuwien.ifs.sge.gui.views.game.match.MatchViewController;
import at.ac.tuwien.ifs.sge.gui.wrapper.TaskWrapper;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import javafx.application.Platform;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;

public class TournamentViewController implements GameView, Initializable {

    public final PublishSubject<Boolean> onTournamentFinishedSubject = PublishSubject.create();
    public final PublishSubject<Long> onTimeLimitMsReceivedSubject = PublishSubject.create();


    private Logger log;
    private ExecutorService threadPool;
    private Tournament tournament;
    private TaskWrapper<List<MatchResult>, Tournament> tournamentTask;
    private TournamentBracket tournamentBracket;
    private boolean showVisualization;
    private boolean createAgentLogFiles;

    private Node consoleNode;
    private CompositeDisposable subscriptions;
    private Disposable timeLimitSubscription;
    private boolean firstMatch = true;

    @FXML
    private AnchorPane bracketAnchorPane;

    @FXML
    private MatchViewController matchViewController;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setup( Logger log, ExecutorService threadPool) {
        this.log = log;
        this.threadPool = threadPool;

        matchViewController.setup(log, threadPool);
    }

    @Override
    public void dispose() {
        subscriptions.dispose();
        if (timeLimitSubscription != null)
            timeLimitSubscription.dispose();
        matchViewController.dispose();
        tournamentBracket.dispose();
    }

    public void start(Tournament tournament, boolean showVisualization, boolean createAgentLogFiles) {
        this.tournament = tournament;
        this.showVisualization = showVisualization;
        this.createAgentLogFiles = createAgentLogFiles;
        firstMatch = true;

        if (tournamentBracket != null) {
            tournamentBracket.dispose();
            bracketAnchorPane.getChildren().clear();
        }
        try {
            tournamentBracket = TournamentBracket.fromTournamentMode(tournament.getMode());
            tournamentBracket.setup(tournament, log);
            bracketAnchorPane.getChildren().add(tournamentBracket.getRootNode());
        } catch (IOException e) {
            log.error("Could not load bracket view!");
            log.printStackTrace(e);
        }

        subscriptions = new CompositeDisposable();
        subscriptions.add(tournament.getNextMatchSubject().subscribe(this::onNextMatch));

        tournamentTask = new TaskWrapper<>(tournament);
        tournamentTask.setOnSucceeded(this::onTournamentFinished);
        tournamentTask.setOnFailed(this::onTournamentFailed);
        threadPool.submit(tournamentTask);
    }

    @Override
    public void stop() {
        tournamentTask.cancel(true);
    }

    @Override
    public void attachEngineConsole(Node consoleNode, boolean withVisualization) {
        this.consoleNode = consoleNode;
        matchViewController.attachEngineConsole(consoleNode, withVisualization);
    }

    private void onNextMatch(Match<?,?> match) {
        Platform.runLater( () -> {
            if (!firstMatch) {
                matchViewController.dispose();
                timeLimitSubscription.dispose();
                if (consoleNode != null)
                    matchViewController.attachEngineConsole(consoleNode, showVisualization);
            } else {
                firstMatch = false;
            }
            timeLimitSubscription = matchViewController.onTimeLimitMsReceivedSubject.subscribe(this::onTimeLimitMsReceived);
            matchViewController.attachToMatch(match, showVisualization, createAgentLogFiles);
        });
    }

    private void onTimeLimitMsReceived(Long timeLimitMs) {
        onTimeLimitMsReceivedSubject.onNext(timeLimitMs);
    }

    private void onTournamentFinished(WorkerStateEvent event) {
        var tournamentResult = tournamentTask.getValue();
        if (tournamentResult != null) {
            log._info();
            log._info(tournament.toTextRepresentation());
            log._info();
        }
        onTournamentFinishedSubject.onNext(true);
    }

    private void onTournamentFailed(WorkerStateEvent event) {
        var e = tournamentTask.getException();
        if (e != null) {
            e.printStackTrace();
        }
    }

}
