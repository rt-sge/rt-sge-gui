package at.ac.tuwien.ifs.sge.gui.views.visualization;

import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.game.match.Match;
import at.ac.tuwien.ifs.sge.core.engine.game.match.TurnBasedMatch;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.Game;
import at.ac.tuwien.ifs.sge.gui.factories.JavaFXVisualizationFactory;
import at.ac.tuwien.ifs.sge.gui.interfaces.JavaFXVisualization;
import at.ac.tuwien.ifs.sge.gui.loader.JavaFXVisualizationLoader;
import at.ac.tuwien.ifs.sge.gui.util.UI;
import at.ac.tuwien.ifs.sge.gui.views.console.Console;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Future;
import java.util.function.Consumer;

public class VisualizationViewController<G extends Game<A, ?>, A> implements Initializable {

    private static final float FRAMES_PER_SECOND = 30;
    private static final float VISUALIZATION_PADDING = 10;

    private Logger log;
    private JavaFXVisualization<G, A> visualization;

    private CompositeDisposable subscriptions;
    private Timeline visualizationTimeline;
    private Future<?> visualizationFuture;

    @FXML
    private AnchorPane contentBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }

    // Game and its Visualization class need to implement the correct interface!
    @SuppressWarnings("unchecked")
    public void setup(Match<?, ?> match, Game<?, ?> game, Logger log) {
        this.log = log;
        subscriptions = new CompositeDisposable();
        var typedGame = (G) game;
        Consumer<A> updateAction = null;

        switch (game.getVisualizationType()) {
            case text:
                var console = new Console();
                UI.setAllAnchors(console, VISUALIZATION_PADDING);
                contentBox.getChildren().add(console);
                updateAction = ignored -> console.setTextKeepScrollPosition(match.getGame().toTextRepresentation());
                updateAction.accept(null);
                break;
            case javafx:
                var visualizationLoader = new JavaFXVisualizationLoader<>(game, log);
                var visualizationFactory = visualizationLoader.newVisualizationFactory();
                var typedVisualizationFactory = (JavaFXVisualizationFactory<G, A>) visualizationFactory;
                visualization = typedVisualizationFactory.newInstance();
                var width = contentBox.getWidth() - 2 * VISUALIZATION_PADDING;
                var height = contentBox.getHeight() - 2 * VISUALIZATION_PADDING;
                var playerNames = match.getGameAgents().values().stream().map(Agent::toString).toArray(String[]::new);
                var visualizationRoot = visualization.initialize(typedGame, playerNames, width, height);
                var hbox = new HBox();
                var vbox = new VBox();
                hbox.setAlignment(Pos.CENTER);
                vbox.setAlignment(Pos.CENTER);
                var vboxContentNode = visualizationRoot;
                if (!visualization.isAutoResizing()) {
                    var scrollPane = new ScrollPane();
                    scrollPane.setContent(visualizationRoot);
                    vboxContentNode = scrollPane;
                }
                vbox.getChildren().add(vboxContentNode);
                hbox.getChildren().add(vbox);
                UI.setAllAnchors(hbox, VISUALIZATION_PADDING);
                contentBox.getChildren().add(hbox);

                contentBox.widthProperty().addListener(this::onViewSizeChanged);
                contentBox.heightProperty().addListener(this::onViewSizeChanged);

                if (game.isRealtime())
                    updateAction = ignored -> visualization.redraw();
                else
                    updateAction = action -> visualization.applyUpdate(action);
                break;
        }

        final Consumer<A> finalUpdateAction = updateAction;
        if (game.isRealtime()) {
            visualizationTimeline = new Timeline(
                    new KeyFrame(
                            Duration.millis(1000 / FRAMES_PER_SECOND),
                            event -> finalUpdateAction.accept(null)));
            visualizationTimeline.setCycleCount(Timeline.INDEFINITE);
            visualizationTimeline.play();
        } else {
            var typedMatch = (TurnBasedMatch<?, A>) match;
            var visualizationUpdateSubject = typedMatch.getVisualizationUpdateSubject();
            subscriptions.add(visualizationUpdateSubject.subscribe( action ->
                Platform.runLater(() -> finalUpdateAction.accept(action))
            ));
        }
    }


    public void dispose() {
        if (visualizationTimeline != null)
            visualizationTimeline.stop();
        else if (subscriptions != null)
            subscriptions.dispose();
        contentBox.getChildren().clear();
    }

    private void onViewSizeChanged(Observable observable, Number oldValue, Number newValue) {
        if (visualization != null)
            visualization.updateViewSize(contentBox.getWidth() - 2 * VISUALIZATION_PADDING, contentBox.getHeight() - 2 * VISUALIZATION_PADDING);
    }

}
