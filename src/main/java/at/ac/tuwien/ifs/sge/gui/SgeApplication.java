package at.ac.tuwien.ifs.sge.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.util.Objects;


public class SgeApplication extends Application {

    public static final String APP_TITLE = "Strategy Game Engine";
    public static final int MIN_WIDTH = 1280;
    public static final int MIN_HEIGHT = 720;

    public static void main(String[] args) {
        launch(args);
    }

    private MainController mainController;

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle(APP_TITLE);
        stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream("logo.png"))));
        var screen = Screen.getPrimary();
        var bounds = screen.getVisualBounds();
        stage.setX(bounds.getMinX());
        stage.setY(bounds.getMinY());
        stage.setWidth(bounds.getWidth());
        stage.setHeight(bounds.getHeight());
        stage.setMinWidth(MIN_WIDTH);
        stage.setMinHeight(MIN_HEIGHT);
        stage.setMaximized(true);

        var fxmlLoader = new FXMLLoader((Objects.requireNonNull(getClass().getResource("main_scene.fxml"))));
        Parent root = fxmlLoader.load();
        mainController = fxmlLoader.getController();
        mainController.setStage(stage);

        Scene scene = new Scene(root);
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("styles.css")).toExternalForm());

        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        mainController.shutdown();
        super.stop();
    }
}
