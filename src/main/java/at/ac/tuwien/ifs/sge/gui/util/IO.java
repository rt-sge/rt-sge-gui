package at.ac.tuwien.ifs.sge.gui.util;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class IO {

    public static File openConfigFileChooser(Stage stage) {
        return getConfigFileChooser().showOpenDialog(stage);
    }

    public static File saveConfigFileChooser(Stage stage) {
        return getConfigFileChooser().showSaveDialog(stage);
    }


    private static FileChooser getConfigFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Board Configuration (*.yaml, *.yml)", "*.yaml", "*.yml"));
        return fileChooser;
    }

}
