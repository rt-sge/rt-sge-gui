package at.ac.tuwien.ifs.sge.gui.views.configuration;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class TurnBasedConfigViewController implements Initializable {

    private static final int DEFAULT_COMPUTATION_TIME = 60;
    private static final int DEFAULT_MAX_ACTIONS = Integer.MAX_VALUE - 1;

    @FXML
    private TextField computationTimeTextField;
    @FXML
    private TextField maxActionsTextField;
    @FXML
    private ChoiceBox<TimeUnit> timeUnitChoiceBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        timeUnitChoiceBox.setItems(FXCollections.observableArrayList(TimeUnit.values()));
        timeUnitChoiceBox.getSelectionModel().select(TimeUnit.SECONDS);
    }

    public int getComputationTime() {
        try {
            return Integer.parseInt(computationTimeTextField.getText());
        } catch (NumberFormatException e) {
            return DEFAULT_COMPUTATION_TIME;
        }
    }

    public int getMaxActions() {
        try {
            return Integer.parseInt(maxActionsTextField.getText());
        } catch (NumberFormatException e) {
            return DEFAULT_MAX_ACTIONS;
        }
    }

    public TimeUnit getTimeUnit() {
        return timeUnitChoiceBox.getValue();
    }
}
