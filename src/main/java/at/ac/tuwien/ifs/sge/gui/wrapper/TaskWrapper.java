package at.ac.tuwien.ifs.sge.gui.wrapper;

import javafx.concurrent.Task;

import java.util.concurrent.Callable;

public class TaskWrapper<K, T extends Callable<K>> extends Task<K> {

    private final T callable;

    public TaskWrapper(T callable) {
        this.callable = callable;
    }

    @Override
    protected K call() throws Exception {
        return callable.call();
    }
}
