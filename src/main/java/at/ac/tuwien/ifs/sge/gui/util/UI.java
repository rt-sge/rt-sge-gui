package at.ac.tuwien.ifs.sge.gui.util;

import at.ac.tuwien.ifs.sge.gui.SgeApplication;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.Optional;

public class UI {
    public static void setAllAnchors(Node node, double value) {
        AnchorPane.setBottomAnchor(node, value);
        AnchorPane.setLeftAnchor(node, value);
        AnchorPane.setTopAnchor(node, value);
        AnchorPane.setRightAnchor(node, value);
    }

    public static Optional<ButtonType> showAlertAndWait(Stage stage, String title, String message, Alert.AlertType type) {
        var alert = new Alert(type);
        alert.setTitle(SgeApplication.APP_TITLE);
        alert.setHeaderText(title);
        var label = new Label(message);
        alert.getDialogPane().setContent(label);
        if (stage == null) alert.initStyle(StageStyle.UTILITY);
        else alert.initOwner(stage);
        return alert.showAndWait();
    }
}

