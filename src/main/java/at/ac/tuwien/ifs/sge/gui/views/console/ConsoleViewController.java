package at.ac.tuwien.ifs.sge.gui.views.console;

import at.ac.tuwien.ifs.sge.gui.helper.RevivablePipedInputStream;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.io.*;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class ConsoleViewController implements Initializable {

    private static final String ANSI_COLOR_CODE_REGEX = "\u001B.{1,3}m+";

    private ExecutorService threadPool;
    private Future<?> readInputStreamFuture;
    private Future<?> readErrorStreamFuture;
    private FileWriter logWriter;

    @FXML
    private Console console;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setup(ExecutorService threadPool, InputStream inputStream, InputStream errorStream, String logFilePath, String name) throws IOException {
        this.threadPool = threadPool;
        if (logFilePath != null) {
            var file = new File(logFilePath);
            logWriter = new FileWriter(file, file.exists());
        }
        readInputStreamFuture = readStream(inputStream, name + " [out]");
        readErrorStreamFuture = readStream(errorStream, name + " [error]");
    }

    public void dispose() {
        readInputStreamFuture.cancel(true);
        readErrorStreamFuture.cancel(true);
        if (logWriter != null) {
            try {
                logWriter.close();
            } catch (IOException ignored) {
            }
        }
    }

    public String readLine() throws InterruptedException {
        return console.readLine();
    }

    public void clearConsole() {
        console.clear();
    }

    private Future<?> readStream(InputStream stream, String name) {
        final BufferedReader reader = new BufferedReader(
                new InputStreamReader(stream));
        return threadPool.submit(new Task<Void>() {
            @Override
            protected Void call() {
                Thread.currentThread().setName("Console Thread " + name);
                try {
                    String line = reader.readLine();
                    while (line  != null) {
                        final String finalLine = line.replaceAll(ANSI_COLOR_CODE_REGEX, "") + "\n";
                        Platform.runLater(() -> {
                            console.appendText(finalLine);
                        });
                        if (logWriter != null)
                            logWriter.write(finalLine);
                        try {
                            line = reader.readLine();
                        } catch (IOException e) {
                            if (stream instanceof RevivablePipedInputStream rpis)
                            {
                                rpis.revive();
                                line = reader.readLine();
                            }
                            else {
                                throw e;
                            }
                        }
                        Thread.sleep(10);
                    }
                } catch (IOException | InterruptedException ignored) {
                }
                return null;
            }
        });
    }
}
