package at.ac.tuwien.ifs.sge.gui.views.console;

import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Font;

public class Console extends TextArea {

    private static final int MAX_LINE_CNT = 500;

    private final Object inputLock = new Object();
    private int lineCount = 0;
    private String userInputString;

    public Console() {

        setFont(new Font("Consolas", 12.0));
        setOnKeyReleased(key -> {
            if (key.getCode() == KeyCode.ENTER) {
                var text = getText();
                text = text.substring(0, text.length() - 1);
                var lineStart = text.lastIndexOf('\n') + 1;
                var input = text.substring(lineStart);
                synchronized (inputLock) {
                    userInputString = input;
                    inputLock.notifyAll();
                }

            }
        });
    }


    @Override
    public void replaceText(int start, int end, String text) {
        String current = getText();
        // only insert if no new lines after insert position:
        if (! current.substring(start).contains("\n")) {
            super.replaceText(start, end, text);
        }
    }

    @Override
    public void replaceSelection(String text) {
        String current = getText();
        int selectionStart = getSelection().getStart();
        if (! current.substring(selectionStart).contains("\n")) {
            super.replaceSelection(text);
        }
    }

    @Override
    public void appendText(String text) {
        var newLines = 0;
        var lineText = text;
        var i = lineText.indexOf('\n');
        while (i >= 0) {
            newLines++;
            lineText = lineText.substring(i + 1);
            i = lineText.indexOf('\n');
        }
        lineCount += newLines;
        if (lineCount > MAX_LINE_CNT) {
            var linesToDelete = lineCount - MAX_LINE_CNT;
            var lastLine = 0;
            while (linesToDelete > 0) {
                lastLine = getText().indexOf('\n', lastLine);
                linesToDelete--;
            }
            super.replaceText(0, lastLine + 1, "");
            lineCount = MAX_LINE_CNT;
        }
        super.appendText(text);
        setScrollTop(Double.MAX_VALUE);
    }


    public void setTextKeepScrollPosition(String value) {
        var scrollTop = getScrollTop();
        var scrollLeft = getScrollLeft();
        super.setText(value);
        setScrollTop(scrollTop);
        setScrollLeft(scrollLeft);
    }

    public String readLine() throws InterruptedException {
        synchronized (inputLock) {
            if (userInputString == null)
                inputLock.wait();
            var line = userInputString;
            userInputString = null;
            return line;
        }
    }

}
