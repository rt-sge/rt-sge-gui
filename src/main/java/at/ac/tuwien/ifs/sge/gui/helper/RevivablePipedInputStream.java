package at.ac.tuwien.ifs.sge.gui.helper;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class RevivablePipedInputStream extends PipedInputStream {

    public RevivablePipedInputStream(PipedOutputStream src) throws IOException {
        super(src);
    }

    public RevivablePipedInputStream(PipedOutputStream src, int pipeSize) throws IOException {
        super(src, pipeSize);
    }

    public void revive() throws IOException {
        receive(32);
    }
}
