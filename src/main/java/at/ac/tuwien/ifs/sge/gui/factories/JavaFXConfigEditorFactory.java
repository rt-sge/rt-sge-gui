package at.ac.tuwien.ifs.sge.gui.factories;

import at.ac.tuwien.ifs.sge.core.engine.factory.Factory;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.gui.interfaces.JavaFXConfigEditor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class JavaFXConfigEditorFactory implements Factory<JavaFXConfigEditor> {

    private final Constructor<JavaFXConfigEditor> configEditorConstructor;
    private final Logger log;
    private final GameConfiguration initialConfiguration;


    public JavaFXConfigEditorFactory(Constructor<JavaFXConfigEditor> configEditorConstructor, GameConfiguration initialConfiguration, Logger log) {
        this.configEditorConstructor = configEditorConstructor;
        this.log = log;
        this.initialConfiguration = initialConfiguration;
    }

    @Override
    public JavaFXConfigEditor newInstance(Object... initargs) {
        try {
            return configEditorConstructor.newInstance(initialConfiguration, log);
        } catch (InstantiationException e) {
            log._error_();
            log.error("Could not instantiate new element with constructor of config editor");
            log.printStackTrace(e);
        } catch (IllegalAccessException e) {
            log._error_();
            log.error("Could not access constructor of config editor");
            log.printStackTrace(e);
        } catch (InvocationTargetException e) {
            log._error_();
            log.error("Could not invoke constructor of config editor");
            log.printStackTrace(e);
        }
        throw new IllegalStateException("JavaFXConfigEditorFactory is faulty");
    }
}
