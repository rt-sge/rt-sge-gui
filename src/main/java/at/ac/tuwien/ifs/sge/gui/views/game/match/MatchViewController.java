package at.ac.tuwien.ifs.sge.gui.views.game.match;

import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.agent.HumanAgent;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.game.MatchResult;
import at.ac.tuwien.ifs.sge.core.engine.game.match.Match;
import at.ac.tuwien.ifs.sge.core.util.Util;
import at.ac.tuwien.ifs.sge.gui.util.UI;
import at.ac.tuwien.ifs.sge.gui.views.console.ConsoleViewController;
import at.ac.tuwien.ifs.sge.gui.views.game.GameView;
import at.ac.tuwien.ifs.sge.gui.views.visualization.VisualizationViewController;
import at.ac.tuwien.ifs.sge.gui.wrapper.TaskWrapper;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class MatchViewController implements GameView, Initializable {

    public final PublishSubject<Boolean> onMatchFinishedSubject = PublishSubject.create();
    public final PublishSubject<Long> onTimeLimitMsReceivedSubject = PublishSubject.create();

    private Logger log;
    private ExecutorService threadPool;
    private TaskWrapper<MatchResult, Match<?, ?>> matchTask;
    private CompositeDisposable subscriptions;
    private boolean showVisualization;
    private boolean createAgentLogFiles;

    private final ArrayList<ConsoleViewController> consoles = new ArrayList<>();
    private final ArrayList<Future<?>> inputForwardingFutures = new ArrayList<>();

    private VisualizationViewController<?, ?> visualizationViewController;
    private AnchorPane visualizationViewRoot;
    private VBox engineLogVbox;
    private Node consoleNode;


    @FXML
    private TabPane consolesTabPane;
    @FXML
    private AnchorPane leftContentAnchorPane;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        var engineLogInfoLabel = new Label("Engine Log:");
        VBox.setMargin(engineLogInfoLabel, new Insets(0, 0, 8.0, 0));
        engineLogVbox = new VBox(engineLogInfoLabel);
        UI.setAllAnchors(engineLogVbox, 10.0);
    }

    public void setup(Logger log, ExecutorService threadPool) {
        this.threadPool = threadPool;
        this.log = log;

        FXMLLoader visualizationViewLoader = new FXMLLoader(VisualizationViewController.class.getResource("visualization_view.fxml"));
        try {
            visualizationViewRoot = visualizationViewLoader.load();
            visualizationViewController = visualizationViewLoader.getController();
        } catch (IOException e) {
            log.error("Could not load visualization view!");
            log.printStackTrace(e);
        }

    }

    public void start(Match<?, ?> match, boolean showVisualization, boolean createAgentLogFiles) {
        attachToMatch(match, showVisualization, createAgentLogFiles);

        matchTask = new TaskWrapper<>(match);
        matchTask.setOnSucceeded(this::onMatchFinished);
        matchTask.setOnFailed(this::onMatchFailed);
        threadPool.submit(matchTask);
    }

    public void attachToMatch(Match<?, ?> match, boolean showVisualization, boolean createAgentLogFiles) {
        this.showVisualization = showVisualization;
        this.createAgentLogFiles = createAgentLogFiles;
        var game = match.getGame();

        if (showVisualization) {
            visualizationViewController.setup(match, game, log);
            leftContentAnchorPane.getChildren().add(visualizationViewRoot);
        }

        subscriptions = new CompositeDisposable();
        subscriptions.add(match.agentStartedSubject.subscribe(this::onAgentStarted));
        subscriptions.add(match.timeLimitMsSubject.subscribe(this::onTimeLimitMsReceived));
    }

    @Override
    public void stop() {
        matchTask.cancel(true);
    }

    @Override
    public void dispose() {
        if (subscriptions != null)
            subscriptions.dispose();
        leftContentAnchorPane.getChildren().clear();
        if (!showVisualization)
            engineLogVbox.getChildren().remove(consoleNode);
        consolesTabPane.getTabs().clear();
        consoles.forEach(ConsoleViewController::dispose);
        consoles.clear();
        inputForwardingFutures.forEach(f -> f.cancel(true));
        inputForwardingFutures.clear();
        visualizationViewController.dispose();
    }

    @Override
    public void attachEngineConsole(Node consoleNode, boolean withVisualization) {
        this.consoleNode = consoleNode;
        if (withVisualization)
            consolesTabPane.getTabs().add(new Tab("Engine Log", consoleNode));
        else {
            VBox.setVgrow(consoleNode, Priority.ALWAYS);
            engineLogVbox.getChildren().add(consoleNode);
            leftContentAnchorPane.getChildren().add(engineLogVbox);
        }
    }

    private void onTimeLimitMsReceived(Long timeLimitMs) {
        onTimeLimitMsReceivedSubject.onNext(timeLimitMs);
    }

    private void onAgentStarted(Agent agent) {
        Platform.runLater(() -> {
            var process = agent.getProcess();
            FXMLLoader consoleViewLoader = new FXMLLoader(ConsoleViewController.class.getResource("console_view.fxml"));
            try {
                AnchorPane consoleViewRoot = consoleViewLoader.load();
                var tab = new Tab(agent.toString() + " Log", consoleViewRoot);
                consolesTabPane.getTabs().add(tab);
                ConsoleViewController consoleViewController = consoleViewLoader.getController();
                consoles.add(consoleViewController);
                String logFilePath = null;
                if (createAgentLogFiles) {
                    logFilePath = agent.redirectOutputToLogFile();
                }
                consoleViewController.setup(threadPool, process.getInputStream(), process.getErrorStream(), logFilePath, agent.getName());

                if (agent instanceof HumanAgent) {
                    var future = forwardConsoleInput(consoleViewController, new PrintStream(process.getOutputStream()));
                    inputForwardingFutures.add(future);
                }
            } catch (IOException e) {
                log.printStackTrace(e);
                log.error("Could not load configuration view!");
            }
        });
    }

    private Future<?> forwardConsoleInput(ConsoleViewController consoleViewController, PrintStream outputStream) {
        return threadPool.submit(new Task<Void>() {
            @Override
            protected Void call() {
                while (true) {
                    try {
                        String line = consoleViewController.readLine();
                        outputStream.println(line);
                        outputStream.flush();
                    } catch (InterruptedException e) {
                        break;
                    }
                }
                return null;
            }
        });
    }

    private void onMatchFinished(WorkerStateEvent event) {
        var matchResult = matchTask.getValue();
        if (matchResult != null) {
            log.infof("Finished match in %s.",
                    Util.convertUnitToMinimalString(matchResult.getDuration(), TimeUnit.NANOSECONDS));
            log._info();
            log._info(matchResult.toString());
            log._info();
        }
        onMatchFinishedSubject.onNext(true);
    }

    private void onMatchFailed(WorkerStateEvent event) {
        var e = matchTask.getException();
        if (e != null) {
            e.printStackTrace();
        }
    }

}
