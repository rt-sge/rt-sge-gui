package at.ac.tuwien.ifs.sge.gui.views.configuration;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class RealtimeConfigViewController implements Initializable {
    private static final int DEFAULT_TIME_LIMIT = 5;

    @FXML
    private TextField timeLimitTextField;
    @FXML
    private ChoiceBox<TimeUnit> timeUnitChoiceBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        timeUnitChoiceBox.setItems(FXCollections.observableArrayList(TimeUnit.values()));
        timeUnitChoiceBox.getSelectionModel().select(TimeUnit.MINUTES);
    }

    public long getTimeLimit() {
        var timeUnit = timeUnitChoiceBox.getValue();
        try {
            var timeLimitAmount = Integer.parseInt(timeLimitTextField.getText());
            return timeUnit.toMillis(timeLimitAmount);
        } catch (NumberFormatException e) {
            return timeUnit.toMillis(DEFAULT_TIME_LIMIT);
        }
    }
}
