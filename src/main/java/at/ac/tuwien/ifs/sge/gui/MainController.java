package at.ac.tuwien.ifs.sge.gui;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.game.match.Match;
import at.ac.tuwien.ifs.sge.core.engine.game.tournament.Tournament;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.core.util.Util;
import at.ac.tuwien.ifs.sge.gui.helper.PrintStreamInterceptor;
import at.ac.tuwien.ifs.sge.gui.interfaces.JavaFXConfigEditor;
import at.ac.tuwien.ifs.sge.gui.util.UI;
import at.ac.tuwien.ifs.sge.gui.views.configuration.ConfigEditorViewController;
import at.ac.tuwien.ifs.sge.gui.views.configuration.ConfigurationViewController;
import at.ac.tuwien.ifs.sge.gui.views.console.ConsoleViewController;
import at.ac.tuwien.ifs.sge.gui.views.game.GameViewController;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

public class MainController implements Initializable {



    private static final long AWAIT_TERMINATION_TIME = 5;
    private static final TimeUnit AWAIT_TERMINATION_TIMEUNIT = TimeUnit.SECONDS;

    private static final String GAME_DIRECTORY_KEY = "game_directory";
    private static final String AGENT_DIRECTORY_KEY = "agent_directory";
    private static final String LOG_FILE_NAME = "sge_log.txt";

    private final Preferences preferences = Preferences.userNodeForPackage(SgeApplication.class);
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    private Logger log;
    private ExecutorService threadPool;

    private Stage stage;

    private ViewType currentView = ViewType.configuration;

    private ConsoleViewController consoleViewController;
    private AnchorPane consoleViewRoot;

    private ConfigurationViewController configurationViewController;
    private HBox configurationViewRoot;

    private ConfigEditorViewController configEditorViewController;
    private GridPane configEditorViewRoot;

    private GameViewController gameViewController;
    private GridPane gameViewRoot;


    private String gameDirectory;
    private String agentDirectory;



    @FXML
    private AnchorPane contentPane;

    @FXML
    private ToggleGroup logLevelToggleGroup;
    @FXML
    private RadioMenuItem logTraceMenuItem;
    @FXML
    private RadioMenuItem logDebugMenuItem;
    @FXML
    private RadioMenuItem logInfoMenuItem;
    @FXML
    private RadioMenuItem logWarnMenuItem;
    @FXML
    private RadioMenuItem logErrorMenuItem;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        var systemOutInterceptor = new PrintStreamInterceptor(System.out);
        var systemErrInterceptor = new PrintStreamInterceptor(System.err);
        System.setOut(systemOutInterceptor);
        System.setErr(systemErrInterceptor);
        setupLogger();
        initializeThreadPool();

        gameDirectory = preferences.get(GAME_DIRECTORY_KEY, null);
        agentDirectory = preferences.get(AGENT_DIRECTORY_KEY, null);

        FXMLLoader consoleViewLoader = new FXMLLoader(ConsoleViewController.class.getResource("console_view.fxml"));
        try {
            consoleViewRoot = consoleViewLoader.load();
            consoleViewController = consoleViewLoader.getController();
            var logDir = Util.createLogDirectory();
            var logFile = logDir + "/" + LOG_FILE_NAME;
            consoleViewController.setup(threadPool, systemOutInterceptor.getInputStream(), systemErrInterceptor.getInputStream(), logFile, "Application");
        } catch (IOException e) {
            log.printStackTrace(e);
            log.error("Could not load match view!");
        }

        FXMLLoader configurationViewLoader = new FXMLLoader(ConfigurationViewController.class.getResource("configuration_view.fxml"));
        try {
            configurationViewRoot = configurationViewLoader.load();
            configurationViewController = configurationViewLoader.getController();
            configurationViewController.attachConsole(consoleViewRoot);
        } catch (IOException e) {
            log.printStackTrace(e);
            log.error("Could not load configuration view!");
        }

        FXMLLoader configEditorViewLoader = new FXMLLoader(ConfigEditorViewController.class.getResource("config_editor_view.fxml"));
        try {
            configEditorViewRoot = configEditorViewLoader.load();
            configEditorViewController = configEditorViewLoader.getController();
        } catch (IOException e) {
            log.printStackTrace(e);
            log.error("Could not load config editor view!");
        }

        FXMLLoader gameViewLoader = new FXMLLoader(GameViewController.class.getResource("game_view.fxml"));
        try {
            gameViewRoot = gameViewLoader.load();
            gameViewController = gameViewLoader.getController();
            gameViewController.setup(log, threadPool);
        } catch (IOException e) {
            log.printStackTrace(e);
            log.error("Could not load match view!");
        }

        initializeLogLevelMenuItems();

        subscriptions.add(configurationViewController.startMatchSubject.subscribe(this::startMatch));
        subscriptions.add(configurationViewController.startTournamentSubject.subscribe(this::startTournament));
        subscriptions.add(configurationViewController.openConfigEditorSubject.subscribe(this::openConfigEditor));
        subscriptions.add(gameViewController.goToConfigurationSubject.subscribe(this::goToConfiguration));
        subscriptions.add(configEditorViewController.closeEditorSubject.subscribe(this::onEditorClosed));

        configurationViewController.loadGameFiles(gameDirectory);
        configurationViewController.loadAgentFiles(agentDirectory);

        setView(ViewType.configuration);
    }

    public void shutdown() {
        subscriptions.dispose();
        consoleViewController.dispose();
        gameViewController.dispose();
        try {
            cleanUpPool();
        } catch (InterruptedException e) {
            log.error_();
            log.error("Interrupted.");
        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
        configurationViewController.setup(stage, log, threadPool);
        configEditorViewController.setup(stage, log);
    }


    public void setView(ViewType viewType) {
        currentView = viewType;
        contentPane.getChildren().clear();
        configurationViewController.detachConsole();
        switch (viewType) {
            case configuration:
                configurationViewController.attachConsole(consoleViewRoot);
                contentPane.getChildren().add(configurationViewRoot);
                break;
            case configEditor:
                contentPane.getChildren().add(configEditorViewRoot);
                break;
            default:
                var withVisualization = configurationViewController.shouldShowVisualization();
                gameViewController.attachEngineConsole(consoleViewRoot, withVisualization);
                contentPane.getChildren().add(gameViewRoot);
                break;
        }
    }

    public void onEditorClosed(GameConfiguration configuration) {
        configurationViewController.loadGame(configuration, "Updated in Editor");
        goToConfiguration(true);
    }

    public void goToConfiguration(boolean value) {
        if (value)
            setView(ViewType.configuration);
    }


    public void setGameDirectory() {
        var newDirectory = setDirectory("Game", GAME_DIRECTORY_KEY);
        if (newDirectory != null) {
            gameDirectory = newDirectory;
            configurationViewController.loadGameFiles(gameDirectory);
        }
    }

    public void setAgentDirectory() {
        var newDirectory = setDirectory("Agent", AGENT_DIRECTORY_KEY);
        if (newDirectory != null) {
            agentDirectory = newDirectory;
            configurationViewController.loadAgentFiles(agentDirectory);
        }
    }

    public void setLogLevel() {
        var selectedLevel = (int) logLevelToggleGroup.getSelectedToggle().getUserData();
        log.setLogLevel(selectedLevel);
    }

    public void openEngineWiki() {
        try {
            var desktop = Desktop.getDesktop();
            var uri = new URI("https://gitlab.com/rt-sge/core/-/wikis/home");
            desktop.browse(uri);
        } catch (URISyntaxException | IOException e) {
            log.error("Could not open Wiki URL!");
            log.printStackTrace(e);
        }
    }

    public void openAboutDialog() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText("Real Time Strategy Engine");
        alert.setContentText("The Real Time Strategy Engine has been developed by Benjamin Beinder and Gabriel Häusle as part of their bachelor thesis (2021-2022) for the Institue of Information Systems Engineering (IFS) at the Technical University of Vienna.");
        alert.initOwner(stage);
        alert.showAndWait();
    }


    private void startMatch(Match<?, ?> match) {
        var showVisualization = configurationViewController.shouldShowVisualization();
        var createAgentLogFiles = configurationViewController.shouldCreateAgentLogFiles();
        gameViewController.startMatch(match, showVisualization, createAgentLogFiles);
        setView(ViewType.match);
    }

    private void startTournament(Tournament tournament) {
        var showVisualization = configurationViewController.shouldShowVisualization();
        var createAgentLogFiles = configurationViewController.shouldCreateAgentLogFiles();
        gameViewController.startTournament(tournament, showVisualization, createAgentLogFiles);
        setView(ViewType.tournament);

    }

    private void openConfigEditor(JavaFXConfigEditor editor) {
        configEditorViewController.showEditor(editor);
        setView(ViewType.configEditor);
    }

    private void initializeThreadPool() {
        threadPool = Executors.newFixedThreadPool(50);
    }

    private void setupLogger() {
        log = new Logger(0, "[sge ", "",
                "trace]: ", System.out, "",
                "debug]: ", System.out, "",
                "info]: ", System.out, "",
                "warn]: ", System.err, "",
                "error]: ", System.err, "");
    }

    private void initializeLogLevelMenuItems() {
        logTraceMenuItem.setUserData(-2);
        logDebugMenuItem.setUserData(-1);
        logInfoMenuItem.setUserData(0);
        logWarnMenuItem.setUserData(1);
        logErrorMenuItem.setUserData(2);
    }

    private String setDirectory(String identifier, String preferencesKey) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        File selectedDirectory = directoryChooser.showDialog(stage);
        if (selectedDirectory != null) {
            var result = UI.showAlertAndWait(stage,"Set new " + identifier + " directory?",
                    "Selected directory is:\n" + selectedDirectory.getAbsolutePath(),
                    Alert.AlertType.CONFIRMATION);
            if (result.isPresent()) {
                if (result.get() == ButtonType.OK) {
                    preferences.put(preferencesKey, selectedDirectory.getAbsolutePath());
                    return selectedDirectory.getAbsolutePath();
                }
            }
        }
        return null;
    }


    private void cleanUpPool() throws InterruptedException {
        if (threadPool == null) {
            return;
        }
        log.tra_("Shutting down ThreadPool");
        threadPool.shutdown();
        log._trace(", done.");
        log.tra_("Waiting " + Util
                .convertUnitToReadableString(AWAIT_TERMINATION_TIME, AWAIT_TERMINATION_TIMEUNIT)
                + " for termination");
        long startTime = System.nanoTime();
        try {
            if (!threadPool.awaitTermination(AWAIT_TERMINATION_TIME, AWAIT_TERMINATION_TIMEUNIT)) {
                log._trace(", failed.");
                log.info("ThreadPool did not yet shutdown. Forcing.");
                List<Runnable> stillRunning = threadPool.shutdownNow();
            } else {
                long endTime = System.nanoTime();
                log._trace(", done in " + Util
                        .convertUnitToMinimalString(endTime - startTime, TimeUnit.NANOSECONDS) + ".");
            }
        } catch (
                InterruptedException e) {
            log._trace(", failed.");
            log.warn("ThreadPool termination was interrupted.");
            throw e;
        }
    }
}
