package at.ac.tuwien.ifs.sge.gui.factories;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.factory.Factory;
import at.ac.tuwien.ifs.sge.core.game.Game;
import at.ac.tuwien.ifs.sge.gui.interfaces.JavaFXVisualization;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class JavaFXVisualizationFactory<G extends Game<A, ?>, A> implements Factory<JavaFXVisualization<G, A>> {

    private final Constructor<JavaFXVisualization<G, A>> visualizationConstructor;
    private final Logger log;


    public JavaFXVisualizationFactory(Constructor<JavaFXVisualization<G, A>> visualizationConstructor, Logger log) {
        this.visualizationConstructor = visualizationConstructor;
        this.log = log;
    }

    @Override
    public JavaFXVisualization<G, A> newInstance(Object... initargs) {
        try {
            var newArgs = new Object[initargs.length + 1];
            System.arraycopy(initargs, 0, newArgs, 0, initargs.length);
            newArgs[initargs.length] = log;
            return visualizationConstructor.newInstance(newArgs);
        } catch (InstantiationException e) {
            log._error_();
            log.error("Could not instantiate new element with constructor of visualization");
            log.printStackTrace(e);
        } catch (IllegalAccessException e) {
            log._error_();
            log.error("Could not access constructor of visualization");
            log.printStackTrace(e);
        } catch (InvocationTargetException e) {
            log._error_();
            log.error("Could not invoke constructor of visualization");
            log.printStackTrace(e);
        }
        throw new IllegalStateException("JavaFXVisualizationFactory is faulty");
    }
}
