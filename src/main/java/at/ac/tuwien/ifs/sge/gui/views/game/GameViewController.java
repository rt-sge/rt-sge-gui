package at.ac.tuwien.ifs.sge.gui.views.game;

import at.ac.tuwien.ifs.sge.core.engine.game.match.Match;
import at.ac.tuwien.ifs.sge.core.engine.game.tournament.Tournament;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.gui.views.game.match.MatchViewController;
import at.ac.tuwien.ifs.sge.gui.views.game.tournament.TournamentViewController;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;

public class GameViewController implements Initializable {

    public final PublishSubject<Boolean> goToConfigurationSubject = PublishSubject.create();

    private final BooleanProperty running = new SimpleBooleanProperty(false);
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    private Logger log;
    ExecutorService threadPool;
    Timer timer;
    private GameViewType viewType;
    private GameView currentGameView;

    private MatchViewController matchViewController;
    private SplitPane matchViewRoot;

    private TournamentViewController tournamentViewController;
    private TabPane tournamentViewRoot;

    private Long currentTimeLimit = 0L;

    @FXML
    private AnchorPane contentAnchorPane;
    @FXML
    private Button backToConfigurationBtn;
    @FXML
    private Button stopBtn;
    @FXML
    private Label remainingTimeLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        backToConfigurationBtn.disableProperty()
                .bind(running);
        stopBtn.disableProperty()
                .bind(running.not());
    }

    public void setup(Logger log,ExecutorService threadPool) {
        this.log = log;
        this.threadPool = threadPool;
        FXMLLoader matchViewLoader = new FXMLLoader(MatchViewController.class.getResource("match_view.fxml"));
        try {
            matchViewRoot = matchViewLoader.load();
            matchViewController = matchViewLoader.getController();
            matchViewController.setup(log, threadPool);
            subscriptions.add(matchViewController.onMatchFinishedSubject.subscribe(this::onGameViewFinished));
            subscriptions.add(matchViewController.onTimeLimitMsReceivedSubject.subscribe(this::onTimeLimitMsReceived));
        } catch (IOException e) {
            log.printStackTrace(e);
            log.error("Could not load match view!");
        }

        FXMLLoader tournamentViewLoader = new FXMLLoader(TournamentViewController.class.getResource("tournament_view.fxml"));
        try {
            tournamentViewRoot = tournamentViewLoader.load();
            tournamentViewController = tournamentViewLoader.getController();
            tournamentViewController.setup(log, threadPool);
            subscriptions.add(tournamentViewController.onTournamentFinishedSubject.subscribe(this::onGameViewFinished));
            subscriptions.add(tournamentViewController.onTimeLimitMsReceivedSubject.subscribe(this::onTimeLimitMsReceived));
        } catch (IOException e) {
            log.printStackTrace(e);
            log.error("Could not load tournament view!");
        }
    }

    public void startMatch(Match<?, ?> match, boolean showVisualization, boolean createAgentLogFiles) {
        viewType = GameViewType.match;
        currentGameView = matchViewController;
        stopBtn.setText("Stop Match");
        contentAnchorPane.getChildren().clear();
        contentAnchorPane.getChildren().add(matchViewRoot);
        running.set(true);
        remainingTimeLabel.setText("--:--");
        matchViewController.start(match, showVisualization, createAgentLogFiles);
    }

    public void startTournament(Tournament tournament, boolean showVisualization, boolean createAgentLogFiles) {
        viewType = GameViewType.tournament;
        currentGameView = tournamentViewController;
        stopBtn.setText("Stop Tournament");
        contentAnchorPane.getChildren().clear();
        contentAnchorPane.getChildren().add(tournamentViewRoot);
        running.set(true);
        remainingTimeLabel.setText("--:--");
        tournamentViewController.start(tournament, showVisualization, createAgentLogFiles);
    }

    public void stop() {
        if (timer != null)
            timer.cancel();
        currentGameView.stop();
        running.set(false);
    }


    public void dispose() {
        if (timer != null)
            timer.cancel();
        disposeGameView();
        subscriptions.dispose();
    }

    public void attachEngineConsole(Node consoleNode, boolean withVisualization) {
        currentGameView.attachEngineConsole(consoleNode, withVisualization);
    }

    public void goToConfiguration() {
        if (timer != null)
            timer.cancel();
        disposeGameView();
        goToConfigurationSubject.onNext(true);
    }

    private void disposeGameView() {
        if (currentGameView != null) {
            if (running.get())
                currentGameView.stop();
            currentGameView.dispose();
        }
    }

    private void onGameViewFinished(Boolean finished) {
        if (timer != null)
            timer.cancel();
        if (finished)
            running.set(false);
    }

    private void onTimeLimitMsReceived(Long timeLimitMs) {
        currentTimeLimit = timeLimitMs;
        scheduleCountdown();
        Platform.runLater(() -> displayRemainingTime(currentTimeLimit));
    }

    private void scheduleCountdown() {
        if (timer != null)
            timer.cancel();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                currentTimeLimit -= 1000;
                if (currentTimeLimit < 0) {
                    currentTimeLimit = 0L;
                    timer.cancel();
                }
                Platform.runLater(() -> {
                    displayRemainingTime(currentTimeLimit);
                });
            }
        }, 1000, 1000);
    }

    private void displayRemainingTime(Long timeMs) {
        var timeSeconds = (int)(timeMs / 1000);
        var hours = timeSeconds / (60 * 60);
        var minutes = (timeSeconds / 60) % 60;
        var seconds = timeSeconds % 60;
        var timeString = String.format("%02d", minutes) + ":" + String.format("%02d", seconds);
        if (hours > 0)
            timeString = String.format("%02d", hours) + ":" + timeString;
        remainingTimeLabel.setText(timeString);
    }
}
