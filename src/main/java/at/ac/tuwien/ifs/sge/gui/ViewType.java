package at.ac.tuwien.ifs.sge.gui;

public enum ViewType {
    configuration,
    match,
    tournament,
    configEditor,
}
