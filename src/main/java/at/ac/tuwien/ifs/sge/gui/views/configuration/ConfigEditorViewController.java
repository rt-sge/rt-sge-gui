package at.ac.tuwien.ifs.sge.gui.views.configuration;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.gui.interfaces.JavaFXConfigEditor;
import at.ac.tuwien.ifs.sge.gui.util.IO;
import at.ac.tuwien.ifs.sge.gui.util.UI;
import io.reactivex.rxjava3.subjects.PublishSubject;
import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ConfigEditorViewController implements Initializable {

    public final PublishSubject<GameConfiguration> closeEditorSubject = PublishSubject.create();

    private Stage stage;
    private Logger log;
    private JavaFXConfigEditor editor;

    @FXML
    AnchorPane contentAnchorPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setup(Stage stage, Logger log) {
        this.stage = stage;
        this.log = log;
    }

    public void showEditor(JavaFXConfigEditor editor) {

        this.editor = editor;

        var width = contentAnchorPane.getWidth();
        var height = contentAnchorPane.getHeight();
        editor.initialize(width, height);
        var editorView = editor.getNode();
        UI.setAllAnchors(editorView, 0);

        contentAnchorPane.getChildren().add(editorView);

        contentAnchorPane.widthProperty().addListener(this::onViewSizeChanged);
        contentAnchorPane.heightProperty().addListener(this::onViewSizeChanged);
    }

    public void closeEditor() {
        var configuration = editor.getConfiguration();
        editor.dispose();
        contentAnchorPane.getChildren().clear();
        closeEditorSubject.onNext(configuration);
    }

    public void openFile() {
        try {
            var file = IO.openConfigFileChooser(stage);
            if (file != null) {
                editor.openFile(file);
            }
        } catch (FileNotFoundException e) {
            UI.showAlertAndWait(stage,"Error while loading config file!", "File could not be opened!", Alert.AlertType.ERROR);
        }
    }

    public void saveFile() {
        try {
            var file = IO.saveConfigFileChooser(stage);
            if (file != null) {
                editor.saveFile(file);
            }
        } catch (IOException e) {
            UI.showAlertAndWait(stage, "Error while saving config file!", "File is a directory!", Alert.AlertType.ERROR);
        }
    }

    private void onViewSizeChanged(Observable observable, Number oldValue, Number newValue) {
        if (editor != null)
            editor.updateViewSize(contentAnchorPane.getWidth(), contentAnchorPane.getHeight());
    }
}
