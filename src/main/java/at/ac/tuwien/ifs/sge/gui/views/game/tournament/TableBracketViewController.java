package at.ac.tuwien.ifs.sge.gui.views.game.tournament;

import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.game.MatchResult;
import at.ac.tuwien.ifs.sge.core.engine.game.tournament.RoundRobin;
import at.ac.tuwien.ifs.sge.core.engine.game.tournament.Tournament;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.util.Util;
import at.ac.tuwien.ifs.sge.core.util.pair.ImmutablePair;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class TableBracketViewController extends TournamentBracket implements Initializable {


    private final CompositeDisposable subscriptions = new CompositeDisposable();

    private final HashMap<ImmutablePair<Agent, Agent>, Label> oneVsOneResultLabels = new HashMap<>();
    private final HashMap<Agent, ImmutablePair<Label, Label>> resultLabels = new HashMap<>();

    private RoundRobin tournament;
    private Logger log;

    @FXML
    private HBox rootHBox;
    @FXML
    private ScrollPane tableScrollPane;
    @FXML
    private GridPane tableGridPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tableScrollPane.minWidthProperty().bind(tableGridPane.widthProperty().add(2));
        tableScrollPane.minHeightProperty().bind(tableGridPane.heightProperty().add(2));
    }

    @Override
    public void setup(Tournament tournament, Logger log) {
        if (tournament instanceof RoundRobin roundRobin) {
            this.tournament = roundRobin;
        } else {
            log.error("Wrong tournament supplied to TableBracketViewController!");
            return;
        }
        this.log = log;
        subscriptions.add(tournament.getNextMatchResultSubject().subscribe(this::onNextMatchResult));

        var nrOfPlayersPerMatch = tournament.getNrOfPlayersPerMatch();
        var agents = tournament.getAgents();

        for (var x = 0; x < agents.size(); x++) {
            var label = newCenteredLabel(agents.get(x).toString());
            tableGridPane.add(label, x + 1, 0);
            if (nrOfPlayersPerMatch == 2) {
                var label2 = new Label(agents.get(x).toString());
                label2.setPadding(new Insets(10.0));
                tableGridPane.add(label2, 0, x + 1);

                for (var y = 0; y < agents.size(); y++) {
                    if (x == y) continue;
                    var pair = new ImmutablePair<>(agents.get(y), agents.get(x));
                    var resultLabel = newCenteredLabel();
                    oneVsOneResultLabels.put(pair, resultLabel);
                    tableGridPane.add(resultLabel, x + 1, y + 1);
                }
            } else {
                if (x == 0) {
                    tableGridPane.add(newCenteredLabel("Score"), 0, 1);
                    tableGridPane.add(newCenteredLabel("Utility"), 0, 2);
                }
                var scoreLabel = newCenteredLabel();
                var utilityLabel = newCenteredLabel();
                tableGridPane.add(scoreLabel, x + 1, 1);
                tableGridPane.add(utilityLabel, x + 1, 2);
                var labels = new ImmutablePair<>(scoreLabel, utilityLabel);
                resultLabels.put(agents.get(x), labels);
            }
        }
    }

    @Override
    public Node getRootNode() {
        return rootHBox;
    }

    @Override
    public void dispose() {
        subscriptions.dispose();
    }

    private Label newCenteredLabel() {
        return newCenteredLabel("");
    }

    private Label newCenteredLabel(String text) {
        var label = new Label(text);
        label.setPadding(new Insets(10.0));
        GridPane.setValignment(label, VPos.CENTER);
        GridPane.setHalignment(label, HPos.CENTER);
        return label;
    }

    private void onNextMatchResult(MatchResult matchResult) {
        var agents = matchResult.getGameAgents();
        if (tournament.isOneVsOne()) {
            var results = tournament.getOneVsOneResult();
            var matchUp = new ImmutablePair<>(agents.get(0), agents.get(1));
            Platform.runLater(() -> {
                oneVsOneResultLabels.get(matchUp).setText(Util.convertDoubleToMinimalString(results.get(matchUp), 3));
            });
        } else {
            var results = tournament.getResult();
            Platform.runLater(() -> {
                for (var agent : agents) {
                    var result = results.get(agent);
                    resultLabels.get(agent).getA().setText(Util.convertDoubleToMinimalString(result.getA(), 3));
                    resultLabels.get(agent).getB().setText(Util.convertDoubleToMinimalString(result.getB(), 3));
                }
            });

        }
    }
}
