package at.ac.tuwien.ifs.sge.gui.interfaces;

import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import javafx.scene.Node;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public interface JavaFXConfigEditor {

    /**
     * Initializes the config editor with a spcified width and height.
     *
     * @param width of the config editor
     * @param height of the config editor
     */
    void initialize(double width, double height);

    /**
     * Gets the JavaFX node representing the config editor
     *
     * @return the JavaFX node of the config editor
     */
    Node getNode();

    /**
     * Gets the current configuration open in the editor
     *
     * @return the current configuration
     */
    GameConfiguration getConfiguration();

    /**
     * Opens the spcified file and tries to load it as game configuration
     *
     * @param file with game configuration
     * @throws FileNotFoundException if the file is not found
     */
    void openFile(File file) throws FileNotFoundException;

    /**
     * Saves the game configuration to the specified file.
     *
     * @param file to be saved to.
     * @throws IOException if the file is a directory.
     */
    void saveFile(File file) throws IOException;

    /**
     * Updates the config editor to the specified size
     *
     * @param width to update to
     * @param height to update to
     */
    void updateViewSize(double width, double height);

    /**
     * Disposes of all closeables.
     */
    void dispose();
}
