package at.ac.tuwien.ifs.sge.gui.interfaces;

import at.ac.tuwien.ifs.sge.core.game.Game;
import javafx.scene.Node;

public interface JavaFXVisualization<G extends Game<A, ?>, A> {

    /**
     * Initializes the visualization with the provided parameters
     *
     * @param game the game to visualize
     * @param playerNames the names of the players
     * @param width the width of the visualization
     * @param height the height of the visualization
     * @return the JavaFX node representing the visualization
     */
    Node initialize(G game, String[] playerNames, double width, double height);

    /**
     * Redraws the visualization.
     */
    void redraw();


    /**
     * Applies the the effects of the action to the visualization.
     *
     * @param action to apply
     */
    void applyUpdate(A action);

    /**
     * If the visualization does not implement updateViewSize this returns false.
     *
     * @return false if the visualization has a fixed size
     */
    default boolean isAutoResizing() { return false; }

    /**
     * Updates the visualization to the specified size
     *
     * @param width to update to
     * @param height to update to
     */
    default void updateViewSize(double width, double height) {}
}
