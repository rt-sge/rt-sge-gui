package at.ac.tuwien.ifs.sge.gui.loader;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.Game;
import at.ac.tuwien.ifs.sge.gui.factories.JavaFXVisualizationFactory;
import at.ac.tuwien.ifs.sge.gui.interfaces.JavaFXVisualization;

import java.lang.reflect.Constructor;
import java.net.URLClassLoader;

public class JavaFXVisualizationLoader<G extends Game<A, ?>, A> {
    private Class<JavaFXVisualization<G, A>> visualizationClass;

    private final String visualizationClassName;

    private final URLClassLoader classLoader;

    private final Logger log;


    public JavaFXVisualizationLoader(G game, Logger log) {
        this.classLoader = (URLClassLoader) game.getClass().getClassLoader();
        this.visualizationClassName = game.getVisualizationClassName();
        this.log = log;
    }

    @SuppressWarnings("unchecked")
    public JavaFXVisualizationFactory<G, A> newVisualizationFactory() {
        log.tra_("Loading visualization class...");
        try {
            visualizationClass = (Class<JavaFXVisualization<G, A>>) classLoader
                    .loadClass(visualizationClassName);

            Constructor<JavaFXVisualization<G, A>> visualizationConstructor = visualizationClass
                    .getConstructor(Logger.class);

            log._trace(", done.");
            return new JavaFXVisualizationFactory<>(visualizationConstructor, log);
        } catch (ClassNotFoundException e) {
            log._trace(", failed.");
            log.error("Could not find class of visualization.");
            log.printStackTrace(e);
            throw new IllegalStateException("Could not find class of visualization.");
        } catch (NoSuchMethodException e) {
            log._trace(", failed.");
            log.error("Could not find required constructors of visualization.");
            log.printStackTrace(e);
            throw new IllegalStateException("Could not find required constructors of visualization.");
        }
    }
}
