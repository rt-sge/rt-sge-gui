package at.ac.tuwien.ifs.sge.gui.wrapper;

import java.io.File;
import java.util.jar.Attributes;

public class FileWrapper {
    private final File file;
    private final Attributes attributes;

    public FileWrapper(File file, Attributes attributes) {
        this.file = file;
        this.attributes = attributes;
    }

    public File getFile() {
        return file;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    @Override
    public String toString() {
        return file.getName();
    }
}
