open module sge.gui {
    requires sge.core;
    requires io.reactivex.rxjava3;
    requires javafx.controls;
    requires javafx.fxml;
    requires java.prefs;
    requires java.desktop;

    exports at.ac.tuwien.ifs.sge.gui.interfaces;
}
