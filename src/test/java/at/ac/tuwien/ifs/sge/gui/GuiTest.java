package at.ac.tuwien.ifs.sge.gui;

import at.ac.tuwien.ifs.sge.gui.helper.PrintStreamInterceptor;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GuiTest {


    @Test
    public void test_system_out_stream_interception() {
        var interceptor = new PrintStreamInterceptor(System.out);
        System.setOut(interceptor);
        System.out.println("foobar");
        var inputStream = interceptor.getInputStream();
        var reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String line = reader.readLine();
            System.err.println(line);
        } catch (IOException ignored) {

        }
    }
}
